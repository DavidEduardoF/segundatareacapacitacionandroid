package segundatarea.capacitacion.seratic.segundatarea;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import Adapter.ListPeliculasAdapter;
import VO.PeliculaVO;

public class PeliculasActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private ListView viewPeliculas;
    private List<PeliculaVO> listPeliculas;
    private ListAdapter adapterPeliculas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peliculas);
        viewPeliculas = (ListView) findViewById(R.id.peliculas);
        listPeliculas=new ArrayList<PeliculaVO>();
        //Llenar los datos
        String[] urls=new String[]{
                "https://www.movie-list.com/img/posters/big/collateralbeauty.jpg",
                "https://www.movie-list.com/img/posters/big/underworldbloodwars.jpg",
                "https://www.movie-list.com/img/posters/big/lifeontheline.jpg",
                "https://www.movie-list.com/img/posters/big/maxsteel.jpg",
                "https://www.movie-list.com/img/posters/big/certainwomen.jpg",
                "https://www.movie-list.com/img/posters/big/mascots.jpg",
        };
        for (int i = 0; i < 6; i++) {
            PeliculaVO p= new PeliculaVO();
            p.setNombre("Pelicula "+i);
            p.setSinopsis("Sinopsis Pelicula "+i);
            p.setUrl_imagen(urls[i]);
            try {
                p.setFechaEstreno(new SimpleDateFormat("dd/MM/yyyy").parse("10/10/2015"));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            listPeliculas.add(p);
        }
        adapterPeliculas=new ListPeliculasAdapter(listPeliculas,this);
        viewPeliculas.setAdapter(adapterPeliculas);
        viewPeliculas.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View view) {
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(PeliculasActivity.this, listPeliculas.get(i).getNombre()+(i%2==0?" es impar":" es par") , Toast.LENGTH_SHORT).show();
    }
}
