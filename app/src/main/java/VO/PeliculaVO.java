package VO;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.Date;

/**
 * Created by david on 12/09/2016.
 */
public class PeliculaVO {
    private String nombre;
    private String url_imagen;
    private String sinopsis;
    private Date fechaEstreno;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrl_imagen() {
        return url_imagen;
    }

    public void setUrl_imagen(String url_imagen) {
        this.url_imagen = url_imagen;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public Date getFechaEstreno() {
        return fechaEstreno;
    }

    public void setFechaEstreno(Date fechaEstreno) {
        this.fechaEstreno = fechaEstreno;
    }
}
