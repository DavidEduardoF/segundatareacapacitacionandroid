package Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import VO.PeliculaVO;
import segundatarea.capacitacion.seratic.segundatarea.R;

/**
 * Created by david on 12/09/2016.
 */
public class ListPeliculasAdapter extends BaseAdapter{

    private List<PeliculaVO> peliculas;
    private Context context;

    public ListPeliculasAdapter(List<PeliculaVO> peliculas, Context context) {
        this.peliculas = peliculas;
        this.context = context;
    }

    @Override
    public int getCount() {
        return peliculas.size();
    }

    @Override
    public Object getItem(int i) {
        return peliculas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v=view;
        if (v==null){
            v=View.inflate(context, R.layout.template_list_peliculas,null);
        }
        TextView nombre= (TextView) v.findViewById(R.id.titulo);
        TextView sinopsis= (TextView) v.findViewById(R.id.sinopsis);
        TextView fechaCreacion= (TextView) v.findViewById(R.id.fecha_creacion);
        ImageView imagen= (ImageView) v.findViewById(R.id.imagen);
        nombre.setText(peliculas.get(i).getNombre());
        sinopsis.setText(peliculas.get(i).getSinopsis());
        fechaCreacion.setText(peliculas.get(i).getFechaEstreno().toString());
        Picasso.with(context).load(peliculas.get(i).getUrl_imagen()).into(imagen);
        return v;
    }
}
